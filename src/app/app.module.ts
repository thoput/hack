import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
 import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';

import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { NewsFeedComponent } from './components/news-feed/news-feed.component';
import { HeaderComponent } from './components/header/header.component';
import { ComplianceDetailsComponent } from './components/compliance-details/compliance-details.component';
import { WorkflowProgressComponent } from './components/compliance-details/workflow-progress/workflow-progress.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
 import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatStepperModule} from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs'
 
import { ReactiveFormsModule  } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { WorkflowDialogComponent } from './components/workflow-dialog/workflow-dialog.component'; 
import {TextFieldModule} from '@angular/cdk/text-field';
 import { BulkUploadComponent } from './components/bulk-upload/bulk-upload.component';
import { HttpClientService } from './services/http-client.service';
import { TableComponent } from './components/table/table.component';

import {MatExpansionModule} from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';

import {MatListModule} from '@angular/material/list';
import { ComplianceTemplateComponent } from './components/compliance-template/compliance-template.component';
import { FileUploadDialogComponent } from './components/compliance-template/file-upload-dialog/file-upload-dialog.component';
import { NewComplianceTemplateComponent } from './components/compliance-template/new-compliance-template/new-compliance-template.component';
import { NotificationComponent } from './components/notification/notification.component';
import { AuditReportComponent } from './components/audit-report/audit-report.component';
import {MatBadgeModule} from '@angular/material/badge';

@NgModule({
  declarations: [
 
    AppComponent,
    CalendarComponent,
    NewsFeedComponent,
    HeaderComponent,
    ComplianceDetailsComponent ,
    WorkflowProgressComponent ,
    WorkflowDialogComponent,
    BulkUploadComponent,
    TableComponent,
    ComplianceTemplateComponent,
    FileUploadDialogComponent,
    NewComplianceTemplateComponent,
    NotificationComponent,
    AuditReportComponent
  ],
  imports: [
    MatBadgeModule,
    MatListModule,
    HttpClientModule,
    MatExpansionModule,
    TextFieldModule,
    MatToolbarModule,
    MatDividerModule,
    MatButtonModule,
    MatGridListModule,
    MatTooltipModule,
    MatChipsModule,
    MatRippleModule,
    MatBottomSheetModule,
    MatSnackBarModule,
    MatCheckboxModule,
    
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    MatStepperModule,
    MatTabsModule,
    MatButtonToggleModule,
     
    FormsModule,
    ReactiveFormsModule ,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    FlatpickrModule.forRoot(),
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    HttpClientModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    AppRoutingModule
  ],
  providers: [MatDatepickerModule,
    MatMomentDateModule,
    HttpClientService,
      ],
  bootstrap: [AppComponent]
})
export class AppModule { }
