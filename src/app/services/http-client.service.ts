import { Injectable } from '@angular/core';
 
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
 
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  baseURL = "http://localhost:8081/";
  public download(templateId, isTemplate): Observable<Blob> {   
    //const options = { responseType: 'blob' }; there is no use of this
        let uri = this.baseURL+'download?isTemplate='+isTemplate+'&id='+templateId;
        // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
        return this.http.get(uri, { responseType: 'blob' });
    }
    
  
 
  constructor(private http: HttpClient) { }
  getFormTemplates(url, id: any) {
    return this.http.get(this.baseURL + url+"?complianceId="+id);
  }
 
  uploadFile(fileToUpload: File) {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(this.baseURL + 'bulkupload', formData, {responseType: 'text'});
  }

  get(url) {
    return this.http.get(this.baseURL + url);
  }

 
  getChallans(url,id) {
    return this.http.get(this.baseURL + url+"?complianceId="+id);
  }
  getChallansForYearMonth(year,month,url) {
    return this.http.get(this.baseURL + url+"?year="+year+"&month="+month);
  }
  getString(url) {
    return this.http.get(this.baseURL + url, {responseType: 'arraybuffer'});
 
  }

  post(url, data) {
    return this.http.post(this.baseURL + url, data);
  }

  delete(url) {
      return this.http.delete(this.baseURL + url, {responseType: 'text'});
  }

  getCompliance(url,id) {
    return this.http.get(this.baseURL + url+"?id="+id);
  }
  saveCompliance(body) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    body = JSON.stringify(body);
    return this.http.post(this.baseURL+'saveCompliance',body,  {headers: headers} );
  }


  uploadFormTemplate(file: File,complianceId,fileId) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post(`${this.baseURL}uploadFile?transaction=false&complianceId=`+complianceId+'&fileId='+fileId+'&templateId=0', formData);

    
  }
}
