import { Component } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ThemePalette } from '@angular/material/core';
import { Subject } from 'rxjs';
import { NotificationComponent } from './components/notification/notification.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  event = new Subject<boolean>();
  background: ThemePalette = undefined;
  color: ThemePalette = undefined;
  title = 'NasdaqCompApp';
  constructor(private _bottomSheet: MatBottomSheet) {
    this.background = 'primary';
    this.color = 'accent';
  }

  addItem(event) {
    this.event.next(event);
  }

  openBottomSheet(): void {
    this._bottomSheet.open(NotificationComponent);
  }
}
