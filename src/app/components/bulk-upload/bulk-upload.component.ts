import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { HttpClientService } from '../../services/http-client.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.css']
})
export class BulkUploadComponent implements OnInit {

  titleFormControl = new FormControl('', [
    Validators.required
  ]);

  sourceFormControl = new FormControl('', [
    Validators.required
  ]);

  pubDateFormControl = new FormControl('', [
    Validators.required
  ]);

  @Output() newItemEvent = new EventEmitter<boolean>();
  fileToUpload: File = null;

  constructor(private httpClient: HttpClientService, private _snackBar: MatSnackBar) { }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  handleClick() {
    this.httpClient.uploadFile(this.fileToUpload).subscribe(data => {
      this.openSnackBar("Loaded the file successfully", "!!!");
      this.newItemEvent.emit(true);
    });
    
    this.fileToUpload = null;
  }

  handleNotificationSubmit() {
    this.httpClient.post('notification', {title: this.titleFormControl.value, source: this.sourceFormControl.value, pubDate: this.pubDateFormControl.value.toDate()}).subscribe(data => {
      console.log(data);
      this.openSnackBar("Notification Saved", "!!!");
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  ngOnInit(): void {
  }

}
