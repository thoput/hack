import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import { HttpClientService } from 'src/app/services/http-client.service';
import * as moment from 'moment';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  notifications;

  constructor(private _bottomSheetRef: MatBottomSheetRef<NotificationComponent>, private httpClient: HttpClientService) {}

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  ngOnInit(): void {
    this.httpClient.get('notification').subscribe(data => {
        this.notifications = data;
    });
  }

  fromNow(date) {
    return moment(date).fromNow();
  }

}
