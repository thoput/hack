import { Component, OnInit } from '@angular/core';
import { HttpClientService } from 'src/app/services/http-client.service';

@Component({
  selector: 'app-news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.css']
})
export class NewsFeedComponent implements OnInit {

  newsList;

  constructor(private httpService: HttpClientService) { }

  ngOnInit(): void {
    this.httpService.get('news').subscribe(data => {
      this.newsList = data;
      console.log(data);
    })
  }

}
