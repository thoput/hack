import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClientService } from 'src/app/services/http-client.service';

@Component({
  selector: 'app-audit-report',
  templateUrl: './audit-report.component.html',
  styleUrls: ['./audit-report.component.css']
})
export class AuditReportComponent implements OnInit {

  displayedColumns: string[] = ['select','complianceName', 'fileName'];

  dataSource;
  selection;
  selectedMonth = 11;
  selectedYear = 2020;

  months = [
    {id: 1, name: 'Jan'},
    {id: 2, name: 'Feb'},
    {id: 3, name: 'Mar'},
    {id: 4, name: 'Apr'},
    {id: 5, name: 'May'},
    {id: 6, name: 'Jun'},
    {id: 7, name: 'Jul'},
    {id: 8, name: 'Aug'},
    {id: 9, name: 'Sep'},
    {id: 10, name: 'Oct'},
    {id: 11, name: 'Nov'},
    {id: 12, name: 'Dec'},
  ];

  years = [2019, 2020, 2021];

  constructor(private httpClient: HttpClientService) { }

  ngOnInit(): void {
    this.httpClient.get('challanForYearMonth?month='+this.selectedMonth+'&year='+this.selectedYear).subscribe(data => {
      let n = [];
      for (const index in data) {
          n.push(data[index]);
      }
      this.dataSource = new MatTableDataSource(n);
      this.selection = new SelectionModel(true, []);
    });
    
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
  }

  update() {
    this.httpClient.get('challanForYearMonth?month='+this.selectedMonth+'&year='+this.selectedYear).subscribe(data => {
      let n = [];
      for (const index in data) {
          n.push(data[index]);
      }
      this.dataSource = new MatTableDataSource(n);
      this.selection = new SelectionModel(true, []);
    });
  }

  download() {
    if(this.selection && this.selection.selected && this.selection.selected.length > 0) {
        console.log(this.selection.selected);
        for (const d in this.selection.selected) {
             const sel = this.selection.selected[d];
             console.log(sel["path"]);
             this.httpClient.getString("file?fileName=" + sel["path"]).subscribe(data => {
              this.createAndDownloadBlobFile(data, sel["fileName"]);
            });
        }
    }
  }

  createAndDownloadBlobFile(body, filename, extension = 'pdf') {
    const blob = new Blob([body]);
    const fileName = `${filename}.${extension}`;
    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, fileName);
    } else {
      const link = document.createElement('a');
      // Browsers that support HTML5 download attribute
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', fileName);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }


}
