import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { map } from 'rxjs/operators';
import { HttpClientService } from 'src/app/services/http-client.service';
import { FileUploadDialogComponent } from './file-upload-dialog/file-upload-dialog.component';
import { NewComplianceTemplateComponent } from './new-compliance-template/new-compliance-template.component';

@Component({
  selector: 'app-compliance-template',
  templateUrl: './compliance-template.component.html',
  styleUrls: ['./compliance-template.component.css']
})
export class ComplianceTemplateComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  templates 
  // = [
  //   {id: 1, name : 'Template 1', recurring: 'Monthly', forms: [{id: 1, name:'Formname', path:"/"}]},
  //   {id: 2, name : 'Template 2', recurring: 'Yearly', forms: [{id: 1, name:'name2', path:"/"}]}
  // ];

  constructor(private dialog: MatDialog, private httpClientService: HttpClientService) {}

  ngOnInit(): void {
    this.getComplianceTemplate();
  }

  private getComplianceTemplate() {
    this.httpClientService.get("complianceTemplate").subscribe(data => {
      this.templates = data;
      this.accordion.openAll();
    });
  }

  openDialog(id) {
    const dialogRef = this.dialog.open(FileUploadDialogComponent, { height: '400px',
    width: '600px', data : {complianceId: id}});

    dialogRef.afterClosed().subscribe(result => {
      this.httpClientService.get("complianceTemplate").subscribe(data => {
        this.templates = data;
      })
    });
  }

  deleteForm(id) {
    this.httpClientService.delete("formTemplate?formId="+id).subscribe(() => {
      this.getComplianceTemplate();
    });
  }

  openDialogNewTemplate() {
    const dialogRef = this.dialog.open(NewComplianceTemplateComponent, { height: '400px',
    width: '600px'});

    dialogRef.afterClosed().subscribe(result => {
      this.httpClientService.get("complianceTemplate").subscribe(data => {
        this.templates = data;
      })
    });
  }

  downloadFile(fileName) {
      this.httpClientService.getString("file?fileName=" + fileName).subscribe(data => {
        this.createAndDownloadBlobFile(data, fileName);
      });
  }

  // base64ToArrayBuffer(bytes) {// Comment this if not using base64
  //   const bytes = new Uint8Array(binaryString.length);
  //   return bytes.map((byte, i) => binaryString.charCodeAt(i));
  // }

  createAndDownloadBlobFile(body, filename, extension = 'pdf') {
    const blob = new Blob([body]);
    const fileName = `${filename}.${extension}`;
    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, fileName);
    } else {
      const link = document.createElement('a');
      // Browsers that support HTML5 download attribute
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', fileName);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

}
