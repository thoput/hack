import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpClientService } from 'src/app/services/http-client.service';

@Component({
  selector: 'app-new-compliance-template',
  templateUrl: './new-compliance-template.component.html',
  styleUrls: ['./new-compliance-template.component.css']
})
export class NewComplianceTemplateComponent implements OnInit {

  name;
  category;

  emailFormControl = new FormControl('', [
    Validators.required
  ]);

  entityFormControl= new FormControl('', [
    Validators.required
  ]);

  constructor(public dialogRef: MatDialogRef<NewComplianceTemplateComponent>, private httpClientService: HttpClientService) { }

  ngOnInit(): void {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  addNewComplianceTemplate() {
    //TODO add new compliance template
    console.log(this.emailFormControl.value);
    this.httpClientService.post("complianceTemplate",{complianceName : this.emailFormControl.value, category: this.entityFormControl.value}).subscribe(data => {
      this.dialogRef.close();
    });
  }

}
