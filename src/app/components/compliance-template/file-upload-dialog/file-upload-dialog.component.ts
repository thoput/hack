import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import { HttpClientService } from 'src/app/services/http-client.service';

@Component({
  selector: 'app-file-upload-dialog',
  templateUrl: './file-upload-dialog.component.html',
  styleUrls: ['./file-upload-dialog.component.css']
})
export class FileUploadDialogComponent implements OnInit {

  formNameAdded
  formId

  fileToUpload: File = null;

  emailFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialogRef: MatDialogRef<FileUploadDialogComponent>, private httpClientService: HttpClientService) { }

  ngOnInit(): void {
    console.log(this.data);
  }

  fileChangeEvent(files: FileList) {
    this.fileToUpload = files.item(0);
    this.httpClientService.uploadFormTemplate(this.fileToUpload, this.data.complianceId, this.formId).subscribe(data => {
      console.log(data);
      this.onNoClick();
    });
    
  }

  addFormName() {
    this.formNameAdded = true;
    this.formId = 1;
    this.httpClientService.post("saveFormTemplateName",{formName: this.emailFormControl.value, complianceId: this.data.complianceId}).subscribe(data => {
      this.formId = data;
      this.formNameAdded = true;
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
