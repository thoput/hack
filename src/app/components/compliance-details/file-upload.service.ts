import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  
  private baseUrl = 'http://localhost:8081';

  constructor(private http: HttpClient) { }

 
  upload(file: File,complianceId,templateId,fileId): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);
if(fileId == undefined){
  fileId = 0;
}
    const req = new HttpRequest('POST', `${this.baseUrl}/uploadFile?transaction=true&complianceId=`+complianceId+'&fileId='+fileId+'&templateId='+templateId, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  uploadChallans(file: File,complianceId,fileId): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}/uploadChallan?complianceId=`+complianceId, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
  getFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/files`);
  }
  export() {
    window.open('/assets/Blurry Link1.pdf', '_blank');
    
}
 
}
