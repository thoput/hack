import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import {FileUploadService} from '../file-upload.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { HttpClientService } from 'src/app/services/http-client.service';
import { MatDialog } from '@angular/material/dialog';
import { WorkflowDialogComponent } from '../../workflow-dialog/workflow-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
 
@Component({
  selector: 'app-workflow-progress',
  templateUrl: './workflow-progress.component.html',
  styleUrls: ['./workflow-progress.component.css'],
  
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class WorkflowProgressComponent implements OnInit {
  @Input() compliance: any;
  forms:any;
  newForms:any;
  constructor(private _snackBar: MatSnackBar,public dialog: MatDialog,private _formBuilder: FormBuilder, private uploadService: FileUploadService,private httpService: HttpClientService) { }
  myfilename = "Select File";
assigneeName:any;
  progress :any;
 
   message : any;
   fileInfos :any;
   challans:any;
   challanmessage:any;
   challansList : any;
   formTemplateList:any;
   test:any;
  ngOnInit() {
   // alert(this.compliance.name);
    this.forms = this.compliance.forms;
     if(this.compliance.assignee != null && this.compliance.assignee  != undefined){
      this.assigneeName = this.compliance.assignee.name;
     }

    this.message = "";
    this.challans = "";
    this.httpService.getChallans("challans",this.compliance.id).subscribe(data => {
      if (data) {
        this.challansList = data;
         }
        
    })
     
 
  }

  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  @Output() newItemEvent = new EventEmitter<boolean>();
  
  downloadFile(data) {
    const blob = new Blob([data]);
    const url= window.URL.createObjectURL(blob);
    window.open(url);
  }

  downloadFileNew(fileName) {
    this.httpService.getString("file?fileName=" + fileName).subscribe(data => {
      this.createAndDownloadBlobFile(data, fileName);
    });
}
downloadFileList(){
  this.challansList.forEach( (element) => {
    this.downloadFileNew(element.path);
});
}

// base64ToArrayBuffer(bytes) {// Comment this if not using base64
//   const bytes = new Uint8Array(binaryString.length);
//   return bytes.map((byte, i) => binaryString.charCodeAt(i));
// }

createAndDownloadBlobFile(body, filename, extension = 'pdf') {
  const blob = new Blob([body]);
  const fileName = `${filename}.${extension}`;
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, fileName);
  } else {
    const link = document.createElement('a');
    // Browsers that support HTML5 download attribute
    if (link.download !== undefined) {
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}
  
  fileChangeEvent(fileInput: any, templateId:any, fileId:any)  {
  
    if (fileInput.target.files && fileInput.target.files[0]) {


      this.myfilename = '';
      Array.from(fileInput.target.files).forEach((file: File) => {
        console.log(file);
       // this.forms[id-1].newFile = file.name ;
       
        const formData: FormData = new FormData();

      formData.append('file', file);
  
      this.uploadService.upload(file,this.compliance.id,templateId,fileId).subscribe(
       
        event => {
          this.progress = 0;
          this.openSnackBar("Loaded the file successfully", "!!!");
        
          this.httpService.getCompliance("compliance",this.compliance.id).subscribe(data => {
            if (data) {
               
              this.compliance = data;
              this.forms = this.compliance.forms;
              this.forms.forEach( (element) => {
                if(element.templateId == templateId){
                 element.newFile =  file.name ;
                // element.message =  'Upload Complete' ;
             
                }
               
           });
              }
          })
      
        });
     
       
      });
      } else {
      this.myfilename = 'Select File';
    }
    
    
  }
  fileChangeEventChallan(fileInput: any)  {
 
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.myfilename = '';
      Array.from(fileInput.target.files).forEach((file: File) => {
        console.log(file);
       
        this.challans = this.challans + file.name+"," ;
        
      this.challansList.push(file);
      this.uploadService.uploadChallans( file,this.compliance.id,1).subscribe(
       
        event => {
          this.httpService.getChallans("challans",this.compliance.id).subscribe(data => {
            if (data) {
              this.challansList = data;
               }
              
          })
         // this.challanmessage =  'Upload Complete';
        
        });
      
      });
     
    } else {
      this.myfilename = 'Select File';
    }
    this.openSnackBar("Uploaded the file successfully", "!!!");
    this.newItemEvent.emit(true);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }
}
