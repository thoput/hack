import {   OnInit , Inject, Input, Output, EventEmitter} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
 
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {Component, NgZone, ViewChild} from '@angular/core';
import {take} from 'rxjs/operators';
import { HttpClientService } from 'src/app/services/http-client.service';
import { FormControl } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
 

interface Statuses {
  id: string;
  viewValue: string;
}
interface Assignee {
  id: number;
  name: string;
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

 
@Component({
  selector: 'app-compliance-details',
  templateUrl: './compliance-details.component.html',
  styleUrls: ['./compliance-details.component.css']
})
export class ComplianceDetailsComponent implements OnInit {
  @Output() newItemEvent = new EventEmitter<boolean>();
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  @Input("selectedEventId") selectedEventId;
 
  comment:any;
  statuses: any;
  assigneeList: any;
   compliance : any;
   modeselect : any;
   assigneeSelect : any;
   serializedDate: any;
   statusId: any;
   assigneeId:any;
   changedDate:Date;
   formTemplateList:any;
   message:any;
  flag:any;
  constructor(private _snackBar: MatSnackBar,public dialog: MatDialog,private _ngZone: NgZone,private httpService: HttpClientService) { }
  ngOnInit() {
    this.flag = false;
    this.httpService.get("users").subscribe(data => {
      if (data) {
        this.assigneeList = data;
         }
        
    })
    this.httpService.get("statuses").subscribe(data => {
      if (data) {
        this.statuses = data;
         }
    })
    this.httpService.getCompliance("compliance",this.selectedEventId).subscribe(data => {
      if (data) {
         
        this.compliance = data;
      
        this.modeselect =  this.compliance.status.id;
       if(this.compliance.assignee!= null && this.compliance.assignee != undefined){
        this.assigneeSelect = this.compliance.assignee.id;
        this.assigneeId =  this.compliance.assignee.id;
       }
        else{
          this.assigneeId =  0;
          this.assigneeSelect = 'Select';
        }
        
        this.serializedDate = new FormControl((new Date( this.compliance.dueDate)).toISOString());
        this.statusId =  this.compliance.status.id;
       
        this.changedDate = new Date(this.compliance.dueDate);

        let dueDate = new Date(this.compliance.dueDate);
        dueDate.setHours(0,0,0,0);
        let currentDate = new Date();
        
        currentDate.setHours(0,0,0,0);
        
       if(dueDate < currentDate && this.compliance.status.name !== 'Completed' ){
        
        this.flag = true;
      }
 
         }
    })

   
  }
  hidden = false;

  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }
  saveCompliance(){
       
    const compliance = {id:  this.compliance.id, dueDate: this.changedDate, statusId:this.statusId, userId:this.assigneeId,comment:this.compliance.comments}
   
    this.httpService.saveCompliance(compliance).subscribe(data => {
      console.log(data);
      if (data) {
           
        this.compliance = data;
      
        this.modeselect =  this.compliance.status.id;
       if(this.compliance.assignee!= null && this.compliance.assignee != undefined){
        this.assigneeSelect = this.compliance.assignee.id;
        this.assigneeId =  this.compliance.assignee.id;
       }
        else{
          this.assigneeId =  0;
          this.assigneeSelect = 'Select';
        }
        
        this.serializedDate = new FormControl((new Date( this.compliance.dueDate)).toISOString());
        this.statusId =  this.compliance.status.id;
       
        this.changedDate = new Date(this.compliance.dueDate);
         }
     

  this.openSnackBar("Successfully updated the compliance", "!!!");
  this.newItemEvent.emit(true);

  });;

         
 }

 openSnackBar(message: string, action: string) {
  let config = new MatSnackBarConfig();
  config.duration = 5000;
  config.panelClass = ['red-snackbar']
  this._snackBar.open(message, action, config);
}
  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  changeStatus(id){
    this.statusId = id;
    
  }
  changeDate(id){
    this.changedDate = id;
    
  }
  changeAssignee(id){
    this.assigneeId = id;
     
  }
        
    
}
 