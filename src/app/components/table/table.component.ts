import { Component, AfterViewInit, Input, SimpleChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClientService } from 'src/app/services/http-client.service';
import { WorkflowDialogComponent } from '../workflow-dialog/workflow-dialog.component';

import jsPDF from "jspdf";
import 'jspdf-autotable';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements AfterViewInit, OnInit {

  displayedColumns = ['name', 'dueDate', 'status', 'assignee'];
  dataSource;
  dataMap = [];
  downloadData =[];

  ngOnInit() {
    this.getCompliance();
    this.event.subscribe(data => {
      if (data) {
        this.getCompliance();
      }
    });
  }

  @Input("event") event: Subject<boolean>;


  constructor(private httpService: HttpClientService, private dialog: MatDialog) {
    
    
   }

  private getCompliance() {
    this.httpService.get("table/compliance").subscribe(data => {
      this.dataMap = [];
      this.downloadData = [];
      for (const index in data) {
        let compliance = data[index]["compliance"];
        let user = data[index]["user"];
        this.dataMap.push({ id: compliance["id"], name: compliance["name"], dueDate: compliance["dueDate"], status: compliance["status"]["name"], assignee: user["name"], email: user["email"] });
        this.downloadData.push({ name: compliance["name"], dueDate: compliance["dueDate"], status: compliance["status"]["name"], comments: compliance["comments"] });
      }
      this.dataSource = new MatTableDataSource(this.dataMap);
    });
  }

  ngAfterViewInit(): void {
    
  }

  openDialog(id): void {
    const dialogRef = this.dialog.open(WorkflowDialogComponent, {
      width: '1000px',
      
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

  download(): void {
    
    var pdf = new jsPDF('landscape', 'px', 'A4');
    
    pdf.autoTable({
      body: this.dataMap,
      columns: [
        { header: 'Compliance', dataKey: 'name' },
        { header: 'Due Date', dataKey: 'dueDate' },
        { header: 'Status', dataKey: 'status' },
        { header: 'Comments', dataKey: 'comments' },
      ],
    });
    

    pdf.save("compliance-calendar.pdf");
}



}

