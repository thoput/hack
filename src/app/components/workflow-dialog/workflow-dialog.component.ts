import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-workflow-dialog',
  templateUrl: './workflow-dialog.component.html',
  styleUrls: ['./workflow-dialog.component.css']
})
export class WorkflowDialogComponent implements OnInit {

  selectedEventId;
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialogRef: MatDialogRef<WorkflowDialogComponent>) { }

  ngOnInit(): void {
    this.selectedEventId = this.data["id"];
  }

}
